package ejemplos.spring.ioc.beans;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import ejemplos.spring.ioc.beans.ServicioRemoto;

public class BeansEjemploXMLTest {

	@Test
	public void pruebaConXmlBeanFactory() {
		BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource(
				"applicationContext.xml"));

		ServicioRemoto servicio1 = beanFactory.getBean("servicioRemoto",
				ServicioRemoto.class);
		System.out.println("El valor es " + servicio1.consultaDato());
	}

	@Test
	public void pruebaConApplicationContext() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"applicationContext.xml");

		ServicioRemoto servicio = applicationContext.getBean("servicioRemoto",
				ServicioRemoto.class);

		System.out.println("El valor es " + servicio.consultaDato());
	}

}

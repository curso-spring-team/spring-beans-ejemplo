package ejemplos.spring.ioc.beansAnotaciones;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;




public class BeansEjemploAnotacionesTest {

	
	@Test
	public void pruebaConApplicationContextAnotacionesXML() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"applicationContextAnotaciones.xml");

		ServicioRemoto servicio = applicationContext.getBean("servicioRemoto",
				ServicioRemoto.class);

		System.out.println("El valor es " + servicio.consultaDato());
	}
	
	@Test
	public void pruebaConApplicationContextAnotaciones() {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext("ejemplos.spring.ioc.beansAnotaciones");
	    ServicioRemoto servicioRemoto = applicationContext.getBean("servicioRemoto", ServicioRemoto.class);

	    System.out.println("El valor es: " + servicioRemoto.consultaDato());
	}
	
	
	

}
